/**
 * Created by simpsop on 28-01-2016.
 */
//import deps
import './base.css!';

//import 'zone.js/dist/zone.min.js';
import 'reflect-metadata'; // reflect-metadata shim is required when using class decorators
import {bootstrap} from "angular2/platform/browser";

import {AppComponent} from "./app-component";

//start our app
bootstrap(AppComponent);

console.log('hello world');

export let __hotReload = true; // mark ES6 file as being live-reloadable