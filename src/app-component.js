import './app-component.css!';
import tpl from './app-component-tpl.html!text';

import {Component} from 'angular2/core';

@Component({
    selector: 'test-app',
    template: tpl
})

export class AppComponent { }

export let __hotReload = true; // mark ES6 file as being live-reloadable