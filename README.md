# JSPM-based toolchain test setup #

A basic test of running an **Angular2** project with a **JSPM**-based toolchain.

* * *

This setup includes:

* Package and dependency management by JSPM (replaces Bower, npm).
* Module loading and management by SystemJS (replaces e.g. RequireJS or Browserify).
* ES6 transpiling by SystemJS.
* CSS & HTML module loading by plugin.
* Hot-reloading JS modules by JSPM dev server.

To-do:

* hot-reloading CSS, HTML.


## Install & setup ##
* * *

Clone this repository, cd into directory.


Install JSPM:

    npm install jspm -g


Create a new project configuration:

    jspm init
*(go with configuration defaults)*


Install module dependencies:

    jspm install angular2 reflect-metadata
    
Install loader plugin dependencies:

    jspm install css
    jspm install text

Set Babel options in config.js like this:

    babelOptions: {
     "optional": [
       "runtime",
       "optimisation.modules.system"
     ],
       "stage": 1
    },


Install development server for JSPM:

    npm install -g jspm-server


Run project:

    jspm-server



## Thoughts on JSPM as a tool ##
* * *

**Pros**

* All-in-one package and dependency management: consumes all module types (ES6, AMD, CommonJS) and repositories (npm, Bower, Github).
* Built in runtime ES6 transpiler (Traceur, TypeScript, Babel).
* Built in bundling for production.
* Hot-reloading JS modules via JSPM dev server.
* Explicit references to CSS dependencies in modules (via CSS loader plugin) and async, runtime injection.

**Cons**

* We can have runtime SCSS transpiling, but it seems nobody has yet really figured out live-reloading of CSS in dev mode.